export interface ICocktail {
    idDrink: number;
    name: string;
    price: number;
    alcoohol: boolean;
    ingredients: string[];
    description: string;
}