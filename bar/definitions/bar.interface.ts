import { ICocktail } from "../../cocktails/definitions/cocktail.interface";
//^ à remplacer par le micro service cocktails

export interface IBar {
    idBar: number;
    location: [number, number];
    addess: string;
    cocktails: ICocktail[];
}